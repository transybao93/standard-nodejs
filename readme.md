## Streaming File Upload or Non-File Upload
1. Busboy

## Request Optimize/Analyze
1. Compression
2. CORS

## Logger
1. Morgan

## Testing
1. Mocha
2. Chai
3. Chai-http
4. ESLint - test coding convention

## Others
1. Husky
2. Moment
3. Lodash
4. Underscore
5. Mongoose plugin

## Validate data
1. Validator

## Database
1. Mongoose

## Mcached
1. memoizee

## Encrypt data / password
1. bcryptjs

## Request Data Handle
1. body-parser

## SMS
1. Twilio

## Queue services
1. Kue

## Email services
1. Nodemailer

## Security
1. helmet

## JWT
1. jsonwebtoken

## OAuth

## OAuth2

